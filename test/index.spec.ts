import { generateInsult, generateInsultFull } from '../src/index';
import { expect } from 'chai';
import 'mocha';

describe('generate function', () => 
{

	it('should return a valid string (long)', () => 
	{
	 	const result = generateInsult();
		expect(result).to.be.a('string');
		expect(result).to.not.be.empty;
  	});
	it('should return a valid string (short, long)', () => 
	{
	 	const result = generateInsultFull();
		expect(result.short).to.be.a('string');
		expect(result.long).to.not.be.empty;
  	});

});
