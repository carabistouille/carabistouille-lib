

export type Insult = { short: string, long: string }

export function generateInsult(): string {
  return makeInsult().long
}

export function generateInsultFull(): Insult {
  return makeInsult()
}


//return an new insult
let makeInsult: () => (Insult)

//tools
let capitalizeFirstLetter: (string: string) => string
let replaceAt: (str: string, index: number, character: string) => (string)
let startWithAVoyelle: (string: string) => boolean

//Database
let accroches: string[] = ["espèce de ", "tu t'es vu avec ta face de ", "fils de ", "fille de ", "rentre chez toi, ", "", "", "", "", "", "rentre chez ta mère, ", "hé, ", "passe ton chemin, ", "parle à ma main, ", "qu'est-ce que tu va faire, ", "je vais t'écraser, ", "ton temps est compté, "]
let accrocheSansArticles: string[] = ["t'es qu'", "t'as la gueule d'", "tu me fait penser à "]
let noms: string[] = ["hippie", "hipster", "communiste", "nazi", "clown", "nerd", "prout", "pirate", "quadrupède", "aztèque", "ivrogne", "macaque", "technocrate", "vampire", "environementaliste", "capitaliste"]
let nomsF: string[] = ["raclure", "pourriture", "chaussette", "mule", "limace", "huître", "horloge murale", "carie", "chèvre", "pile", "crème pâtissière", "annesse", "niche", "camionnette", "machine à laver", "baignoire", "pastèque", "bossue", "chauve-souris", "flaque", "luge", "gourgandine", "antiquité", "caisse", "pantoufle", "cornemuse", "patate", "végétarienne", "perruche", "farceuse", "boite de conserve", "graine", "brouette", "scélérate", "délinquante", "levure", "gorge", "machine à écrire", "mononucléose", "mamie", "gobeuse de mouches", "courgette", "aubergine", "patate douce", "omelette", "chaise"]
let nomsM: string[] = ["canoe", "jambon", "hibou", "caniveau", "gaufrier", "biscuit", "montagnard", "goblin", "mangeur de tarte", "dromadaire", "bâteau à voile", "fakir", "monosourcil", "pneu creuvé", "fond de placard", "poil de nez", "corps au pied", "accordéon", "poney", "sac à douche", "égoutoir", "zèbre", "clairon", "post-it", "robinet", "bossu", "rouleau à pâtisserie", "igloo", "benne à ordure", "chiffon", "poulpe", "fémur", "ravioli", "apache", "athlète", "bandit", "bibelot", "cornichon", "chaufard", "cigare", "diplodocus", "galopin", "hydrocarbure", "tapis", "topinambour", "végétarien", "farceur", "mille-pattes", "chercheur", "convoyeur", "scélérat", "délinquant", "loubard", "orifice", "enzyme", "épouvantail", "lombric", "brouillon", "déserteur", "vendeur de mort", "papy", "gobeur de mouches", "clocher", "manant", "mécréant", "hamster", "porte-manteau", "champignon", "potiron", "parapluie", "citron", "cucurbitacé", "chou-fleur", "brocoli", "épinard", "tiroir", "bureau", "salsifis"]
let adjectifs: string[] = ["étrange", "communiste", "de mes deux", "stupide", "ignoble", "aveugle", "immense", "zinzin", "sans fond", "convexe", "en plastique", "de mauvaise famille", "verdâtre", "analphabète", "mégalomane", "ramollie", "interplanétaire", "en chaussette", "bancale", "indéchiffrable", "angoissant", "lubrique", "tolérable", "vulgaire", "minable", "négligeable", "pas pire", "convenable", "cataclysmique", "catastrophique", "boute-en-train", "épouventable", "innommable", "incrédule", "névralgique", "en carton", "d'eau douce", "dégeulasse", "médiocre", "pas trop drole", "pas cool", "égoiste", "malhonnête", "rectangulaire", "capitaliste", "lamentable", "sporadique"]
let adjectifsF: string[] = ["puante", "ideuse", "dégénérée", "pas belle", "bruyante", "usagée", "trouée", "dissonante", "pas fraiche", "manquée", "ancienne", "pas terminée", "bossue", "cabossée", "boueuse", "dévergondée", "maquillée", "volante", "hurluberlue", "vénéneuse", "écervelée", "revendicatrice", "absolue", "recyclée", "pas satisfaisante", "disjonctée", "ennuyante", "insuffisante", "insignifiante", "infernale", "satelisée", "lyophilisée", "déshydratée", "assistée", "bigarée", "hâtive", "méchante", "defectueuse", "blonde", "paresseuse", "bavarde", "rutilante", "spongieuse"]
let adjectifsM: string[] = ["puant", "ideux", "dégénéré", "pas beau", "bruyant", "usagé", "troué", "dissonant", "pas frais", "manqué", "ancien", "pas terminé", "bossu", "cabossé", "boueux", "niais", "dévergondé", "maquillé", "volant", "hurluberlu", "vénéneux", "écervelé", "revendicateur", "absolu", "recyclé", "pas satisfaisant", "disjoncté", "ennuyant", "insuffisant", "insignifiant", "infernal", "satelisé", "lyophilisé", "déshydraté", "assisté", "bigaré", "hâtif", "méchant", "defectueux", "blond", "paresseux", "bavard", "rutilant", "spongieux"]
let allAdjectifs: string[] = adjectifs.concat(adjectifsF).concat(adjectifsM)

capitalizeFirstLetter = (string: string): string => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

replaceAt = (str: string, index: number, character: string): string => {
  return str.substr(0, index) + character + str.substr(index + character.length)
}

startWithAVoyelle = (string: string): boolean => {
  if (string.charAt(0) == "a" ||
    string.charAt(0) == "e" ||
    string.charAt(0) == "i" ||
    string.charAt(0) == "o" ||
    string.charAt(0) == "u" ||
    string.charAt(0) == "h") {
    return true
  }
  return false
}


makeInsult = (): Insult => {
  let insult: Insult = { short: "", long: "" }
  let statement: string = ""

  let accroche: string = accroches[Math.floor(Math.random() * accroches.length)]
  let accrocheSansArticle: string = accrocheSansArticles[Math.floor(Math.random() * accrocheSansArticles.length)]
  let nom: string = noms[Math.floor(Math.floor(Math.random() * noms.length))]
  let nomF: string = nomsF[Math.floor(Math.random() * nomsF.length)]
  let nomM: string = nomsM[Math.floor(Math.random() * nomsM.length)]
  let adjectif: string = adjectifs[Math.floor(Math.random() * adjectifs.length)]
  let allAdjectif: string = allAdjectifs[Math.floor(Math.random() * allAdjectifs.length)]
  let adjectifF: string = adjectifsF[Math.floor(Math.random() * adjectifsF.length)]
  let adjectifM: string = adjectifsM[Math.floor(Math.random() * adjectifsM.length)]

  let needApostropheNom: boolean = false
  let needApostropheNomF: boolean = false
  let needApostropheNomM: boolean = false

  if ((accroche.charAt(accroche.length - 2) == "e")
    ||
    (accroche.charAt(accroche.length - 2) == "a")) {
    if (startWithAVoyelle(nom)) {
      needApostropheNom = true
    }
    if (startWithAVoyelle(nomF)) {
      needApostropheNomF = true
    }
    if (startWithAVoyelle(nomM)) {
      needApostropheNomM = true
    }

  }

  if (Math.floor(Math.random() * 6) == 0) {
    switch (Math.floor(Math.random() * 4)) {
      case 0:
        accrocheSansArticle = "" + accrocheSansArticle + "un "
        statement = "" + nomM + " " + adjectifM
        break
      case 1:
        accrocheSansArticle = "" + accrocheSansArticle + "une "
        statement = "" + nomF + " " + adjectifF
        break
      case 2:
        accrocheSansArticle = "" + accrocheSansArticle + "un "
        statement = "" + nom + " " + adjectifM
        break
      case 3:
        accrocheSansArticle = "" + accrocheSansArticle + "une "
        statement = "" + nom + " " + adjectifF
        break
    }
    insult.short = statement
    statement = "" + accrocheSansArticle + statement
  }
  else {
    switch (Math.floor(Math.random() * 3)) {
      case 0:
        if (needApostropheNom) {
          accroche = replaceAt(accroche, accroche.length - 2, "'")
          accroche = accroche.slice(0, -1)
        }
        statement = "" + nom + " " + allAdjectif
        break
      case 1:
        if (needApostropheNomM) {
          accroche = replaceAt(accroche, accroche.length - 2, "'")
          accroche = accroche.slice(0, -1)
        }
        statement = "" + nomM + " " + adjectifM
        break
      case 2:
        if (needApostropheNomF) {
          accroche = replaceAt(accroche, accroche.length - 2, "'")
          accroche = accroche.slice(0, -1)
        }
        statement = "" + nomF + " " + adjectifF
        break
    }
    insult.short = statement
    statement = "" + accroche + statement

  }


  statement += " !"
  statement = capitalizeFirstLetter(statement)

  insult.long = statement

  return insult

}
